﻿namespace EnumTypes
{
    public enum Housing
    {
        Shelter,
        Family,
        Rent,
        None
    }
}
